package piglatintranslator;

public class Translator {
	
	private String phrase;
	public static final String NIL = "nil";
	public static final String VOWELS = "aeiou";
	private String[] phrases;
	public static final String PUNCTUATIONS = ".,;:?!'()";


	public Translator(String inputphrase) {
		phrase = inputphrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() throws PigLatinException {
		if(containsUpperCaseLetter()) {
			if(countUpperLetters()==phrase.length()) {
				return translatePhraseWithUpperLetters();
			}
			else if (countUpperLetters()== 1 && isTitleCase()) {
				return translatePhraseWithTitleCase();
			}
			else {
				throw new PigLatinException("Valore non valido");
			}
		}
		if(startWithVowel()) {
			if(phrase.endsWith("y")){
				return phrase + "nay";
			}else if(endWithVowel()) {
				return phrase + "yay";
			} else if (!endWithVowel()) {
				return phrase + "ay";
			}
		}else if(!startWithVowel() && !phrase.isEmpty()){
			String consonanti = phrase.substring(0, indexOfFirstVowel());
			return phrase.substring(indexOfFirstVowel()) + consonanti + "ay";
		}
		return NIL;
	}
	
	public String translatePhrases() throws PigLatinException {
		if(phrase.contains("-")) {
			phrases = phrase.split("-");
			return translatePhrasesSplittedByMinus();

		}else {
			phrases = phrase.split("\\s+");
			return translatePhrasesSplittedByWhiteSpace();
		}

	}


	private boolean startWithVowel() {
		return phrase.startsWith("a")|| phrase.startsWith("e")|| phrase.startsWith("i")|| phrase.startsWith("o")|| phrase.startsWith("u");
	}
	
	private boolean endWithVowel() {
		return phrase.endsWith("a")|| phrase.endsWith("e")|| phrase.endsWith("i")|| phrase.endsWith("o")|| phrase.endsWith("u");
	}
	
	private int indexOfFirstVowel() {
		for(int i=0; i<phrase.length(); i++ ) {
			if(VOWELS.contains(String.valueOf(phrase.charAt(i)))) {
				return i;
			}
		}
		return -1;
	}
	
	private boolean isWithPunctuation() {
		for(int i=0; i<phrase.length(); i++) {
			if(PUNCTUATIONS.contains(String.valueOf(phrase.charAt(i)))) {
				return true;
			}
		}
		return false;
	}	
	
	private int indexOfPunctuation() {
		for(int i=0; i<phrase.length(); i++) {
			if(PUNCTUATIONS.contains(String.valueOf(phrase.charAt(i)))) {
				return i;
			}
		}
		return -1;
	}

	
	private String getPunctuation() {
		return String.valueOf(phrase.charAt(indexOfPunctuation()));
	}
	
	private String translatePhrasesSplittedByMinus() throws PigLatinException {
		StringBuilder translatedPhrases = new StringBuilder();
		for(int i=0; i<phrases.length; i++) {
			phrase = phrases[i];
			if(isWithPunctuation()) {
				String punctuation = getPunctuation();
				phrase = phrase.replace(punctuation, "");
				translatedPhrases.append(this.translate());
				translatedPhrases.append(punctuation);
				}else {
					translatedPhrases.append(this.translate());	
			if (i<phrases.length-1) {
				translatedPhrases.append("-");
				}
			}
		} 
		return translatedPhrases.toString();
	}
	
	private String translatePhrasesSplittedByWhiteSpace() throws PigLatinException {
		StringBuilder translatedPhrases = new StringBuilder();
		for(int i=0; i<phrases.length; i++) {
			phrase = phrases[i];
			if(isWithPunctuation()) {
				String punctuation = getPunctuation();
				phrase = phrase.replace(punctuation, "");
				translatedPhrases.append(this.translate());
				translatedPhrases.append(punctuation);
				}else {
					translatedPhrases.append(this.translate());	
			}
			if (i<phrases.length-1) {
				translatedPhrases.append(" ");
			}
		} 
		return translatedPhrases.toString();
	}
	
	private boolean containsUpperCaseLetter() {
		for(int i=0; i< phrase.length(); i++) {
			if(Character.isUpperCase(phrase.charAt(i))) {
				return true;
			}
		}
		return false;
	}
	
	private int countUpperLetters() {
		int count=0;
		for(int i=0; i<phrase.length();i++) {
			if(Character.isUpperCase(phrase.charAt(i))) {
				count++;
			}
		}
		return count;
	}
	
	private String translatePhraseWithUpperLetters() throws PigLatinException {
		phrase = phrase.toLowerCase();
		phrase = this.translate();
		return phrase.toUpperCase();
		
	}
	
	private String translatePhraseWithTitleCase() throws PigLatinException {
		phrase = phrase.toLowerCase();
		phrase = this.translate();
		return phrase.substring(0,1).toUpperCase() + phrase.substring(1);
	}
	
	private boolean isTitleCase() {
		return(Character.isUpperCase(phrase.charAt(0)));
	}
}
