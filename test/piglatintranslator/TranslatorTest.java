package piglatintranslator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputphrase = "hello world";
		Translator translator = new Translator(inputphrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() throws PigLatinException {
		String inputphrase = "";
		Translator translator = new Translator(inputphrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelAndEndingWithY() throws PigLatinException {
		String inputphrase = "any";
		Translator translator = new Translator(inputphrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithUAndEndingWithY() throws PigLatinException {
		String inputphrase = "utility";
		Translator translator = new Translator(inputphrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelAndEndingWithVowel() throws PigLatinException  {
		String inputphrase = "apple";
		Translator translator = new Translator(inputphrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelAndEndingWithConsonat() throws PigLatinException {
		String inputphrase = "ask";
		Translator translator = new Translator(inputphrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithConsonant() throws PigLatinException  {
		String inputphrase = "hello";
		Translator translator = new Translator(inputphrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonant() throws PigLatinException  {
		String inputphrase = "known";
		Translator translator = new Translator(inputphrase);
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationPhrasesWithWhiteSpace() throws PigLatinException {
		String inputphrase = "hello world";
		Translator translator = new Translator(inputphrase);
		assertEquals("ellohay orldway", translator.translatePhrases());
	}
	
	@Test
	public void testTranslationPhrasesWithMinus() throws PigLatinException {
		String inputphrase = "well-being";
		Translator translator = new Translator(inputphrase);
		assertEquals("ellway-eingbay", translator.translatePhrases());
	}
	
	@Test
	public void testTranslationPhrasesWithWhiteSpaceAndExclamationMark() throws PigLatinException {
		String inputphrase = "hello world!";
		Translator translator = new Translator(inputphrase);
		assertEquals("ellohay orldway!", translator.translatePhrases());
	}
	
	@Test
	public void testTranslationPhrasesWithWhiteSpaceAndPoint() throws PigLatinException {
		String inputphrase = "hello world.";
		Translator translator = new Translator(inputphrase);
		assertEquals("ellohay orldway.", translator.translatePhrases());
	}
	
	@Test
	public void testTranslationPhrasesWithWhiteSpaceAndComma() throws PigLatinException {
		String inputphrase = "hello world,";
		Translator translator = new Translator(inputphrase);
		assertEquals("ellohay orldway,", translator.translatePhrases());
	}
	
	@Test
	public void testTranslationPhrasesWithWhiteSpaceAndSemiColon() throws PigLatinException {
		String inputphrase = "hello world;";
		Translator translator = new Translator(inputphrase);
		assertEquals("ellohay orldway;", translator.translatePhrases());
	}
	
	@Test
	public void testTranslationPhrasesWithWhiteSpaceAndColon() throws PigLatinException {
		String inputphrase = "hello world:";
		Translator translator = new Translator(inputphrase);
		assertEquals("ellohay orldway:", translator.translatePhrases());
	}
	
	@Test
	public void testTranslationPhrasesWithWhiteSpaceAndQuestionMark() throws PigLatinException {
		String inputphrase = "hello world?";
		Translator translator = new Translator(inputphrase);
		assertEquals("ellohay orldway?", translator.translatePhrases());
	}
	
	@Test
	public void testTranslationPhrasesWithWhiteSpaceAndApostrophe() throws PigLatinException {
		String inputphrase = "hello world'";
		Translator translator = new Translator(inputphrase);
		assertEquals("ellohay orldway'", translator.translatePhrases());
	}
	
	@Test
	public void testTranslationPhrasesWithWhiteSpaceAndRoundParenthesisOpen() throws PigLatinException {
		String inputphrase = "hello world(";
		Translator translator = new Translator(inputphrase);
		assertEquals("ellohay orldway(", translator.translatePhrases());
	}
	
	@Test
	public void testTranslationPhrasesWithWhiteSpaceAndRoundParenthesisClosed() throws PigLatinException {
		String inputphrase = "hello world)";
		Translator translator = new Translator(inputphrase);
		assertEquals("ellohay orldway)", translator.translatePhrases());
	}
	
	@Test
	public void testTranslationPhrasesWithUpperCase() throws PigLatinException {
		String inputphrase = "APPLE";
		Translator translator = new Translator(inputphrase);
		assertEquals("APPLEYAY", translator.translate());
	}
	
	@Test
	public void testTranslationPhrasesWithTitleCase() throws PigLatinException {
		String inputphrase = "Hello";
		Translator translator = new Translator(inputphrase);
		assertEquals("Ellohay", translator.translate());
	}
	
	@Test(expected = PigLatinException.class)
	public void testTranslationPhrasesWithRandomLetterUpperCase() throws PigLatinException {
		String inputphrase = "biRd";
		Translator translator = new Translator(inputphrase);
		translator.translate();
	}
	

	
	
	


}
